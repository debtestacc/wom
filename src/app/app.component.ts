import { Component, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { DrawerService } from './services/drawer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'wom-stellar';
  isLoggedIn = "true";
  sideDrawerOpened = false;
  id: string;
  role: string;
  //how to subscribe route value and assign to route property.
  route = "";

  constructor(private router: Router, private authService: AuthService,private drawerService: DrawerService) {
  }

  ngOnInit() {
    //this.isLoggedIn = localStorage.getItem('isLoggedIn');
    this.id = localStorage.getItem('token');
    this.role = localStorage.getItem('role');
    this.authService.currentStatus$.subscribe((status) => this.isLoggedIn = status)
    
    // forcefully changing the value of is logged in 
    setTimeout(() => {
      this.route = this.router.url;
      if (this.route === "/login" || this.route === "/register") {
        this.isLoggedIn = "false";
      }
      else {
        this.isLoggedIn = "true";
      }
    }, 20);
    this.drawerService.drawerStatus.subscribe(status => this.sideDrawerOpened = status);
  }
  log(state) {
    console.log(state)
  }
  // temp signout button in app component
  logout() {
    this.authService.logout();
    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    this.router.navigate(['/login']);
  }

}
