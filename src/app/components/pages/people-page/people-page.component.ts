import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PeopleFormComponent } from '../../forms/people-form/people-form.component';
import { TeamFormComponent } from '../../forms/team-form/team-form.component';
import { TeamEditFormComponent } from '../../forms/team-form/team-edit-form.component';

@Component({
  selector: 'app-people-page',
  templateUrl: './people-page.component.html',
  styleUrls: ['./people-page.component.scss']
})
export class PeoplePageComponent implements OnInit {
  pageTitle = 'People and Teams';
  viewMode = 'tab1';
  pageMenus = ['Import'];
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  openPeopleDialog() {
    const dialogRef = this.dialog.open(PeopleFormComponent, {
      width: '600px',
      height: '800px'
    })
  }
  openTeamDialog() {
    const dialogRef = this.dialog.open(TeamFormComponent, {
      width: '600px',
    })
  }
  openTeamEditDialog() {
    const dialogRef = this.dialog.open(TeamEditFormComponent, {
      width: '600px',
    })
  }
}