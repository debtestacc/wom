import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CategoryFormComponent } from '../../forms/category-form/category-form.component';
import { CategoryFormDataService } from 'src/app/services/category-form-data.service';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss']
})
export class CategoriesPageComponent implements OnInit {
  pageTitle = "Categories";
  //tabs from categories page should be bound to the categories form viewMode to push value to respective 
  viewMode = "tab1";
  workOrderCategories=[];
  assetStatus = [];
  purchaseOrders = [];
  meters = [];
  
  constructor( public dialog: MatDialog, private categoryFormDataService: CategoryFormDataService) { }

  ngOnInit() {
   this.workOrderCategories = this.categoryFormDataService.workOrderCategories;
   this.assetStatus = this.categoryFormDataService.assetStatus;
   this.purchaseOrders = this.categoryFormDataService.purchaseOrders;
   this.meters = this.categoryFormDataService.meters;
  }
  openDialog(){
    const dialogRef = this.dialog.open(CategoryFormComponent);
    this.categoryFormDataService.categorySubject.next(this.viewMode);
  }

  
}
