import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assistant-page',
  templateUrl: './assistant-page.component.html',
  styleUrls: ['./assistant-page.component.scss']
})
export class AssistantPageComponent implements OnInit {
  pageTitle = "Assistant";
  constructor() { }

  ngOnInit() {
  }

}
