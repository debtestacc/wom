import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-automated-workflows-tab-page',
  templateUrl: './automated-workflows-tab-page.component.html',
  styleUrls: ['./automated-workflows-tab-page.component.scss']
})
export class AutomatedWorkflowsTabPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
