import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-general-settings-tab-page',
  templateUrl: './general-settings-tab-page.component.html',
  styleUrls: ['./general-settings-tab-page.component.scss']
})
export class GeneralSettingsTabPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
