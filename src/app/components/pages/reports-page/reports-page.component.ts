import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CustomReportFormComponent } from '../../forms/custom-report-form/custom-report-form.component';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-reports-page',
  templateUrl: './reports-page.component.html',
  styleUrls: ['./reports-page.component.scss']
})
export class ReportsPageComponent implements OnInit {
  pageTitle = "Reports";
  viewMode: String = "tab1";
  customReports = [];
  customReportFormDialogRef: MatDialogRef<CustomReportFormComponent>;
  constructor(private dialog: MatDialog) { }

  ngOnInit() { }

  dashboards = [
    { name: 'foo.js', content: '' },
    { name: 'bar.js', content: '' }
  ];

  openDialog(file?) {
    this.customReportFormDialogRef = this.dialog.open(CustomReportFormComponent, {
      data: {
        filename: file ? file.name : ''
      },
      width:'400px'
    });

    this.customReportFormDialogRef.afterClosed().pipe(
      filter(name => name)
    ).subscribe(name => {
      if (file) {
        const index = this.dashboards.findIndex(f => f.name == file.name);
        if (index !== -1) {
          this.dashboards[index] = { name, content: file.content }
        }
      } else {
        this.dashboards.push({ name, content: '' });
      }
    });
  }

}
