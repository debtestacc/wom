import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  path: string;
  constructor(private router: Router) { }

  ngOnInit() {
    //to change the behaviour of template for setting the nav-item to active
    $('.nav-item').on('click', function () {
      $(this).siblings().removeClass('active');
    });
    this.path = this.router.url;

  }

}
