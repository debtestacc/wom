import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DrawerService {
  sideDrawerOpened: boolean;
  private drawerSubject = new BehaviorSubject(false);
  drawerStatus = this.drawerSubject.asObservable();
  constructor() { }
  toggleStatus() {
    this.drawerSubject.next(!this.sideDrawerOpened);
  }
  getStatus(){
    this.drawerStatus.subscribe(x => this.sideDrawerOpened = x);
  }
}

